package server

import (
	"context"
	"terraform-provider-pterodactyl/provider/resources"
	"terraform-provider-pterodactyl/provider/structs"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/vicanso/go-axios"
)

func deleteServer(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	providerCtx := m.(*structs.ProviderContext)
	res, _ := axios.Request(&axios.Config{
		Method:  "DELETE",
		URL:     providerCtx.Url + "/api/application/servers/" + d.Id() + "/force",
		Headers: resources.GetHeaders(providerCtx.ApiKeys.AppKey),
	})
	if res.Status != 204 {
		return diag.Errorf("api responded with status code %d with response:\n%s", res.Status, res.Data)
	}
	return diag.Diagnostics{}
}
