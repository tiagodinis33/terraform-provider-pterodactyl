package server

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"terraform-provider-pterodactyl/provider/resources"
	"terraform-provider-pterodactyl/provider/structs"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/vicanso/go-axios"
)

func atoi(str string) int {
	result, err := strconv.Atoi(str)
	if err != nil {
		panic(err.Error())
	}
	return result
}
func createServer(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	providerCtx := m.(*structs.ProviderContext)
	body := map[string]interface{}{
		"name":         d.Get("name").(string),
		"description":  d.Get("description"),
		"egg":          d.Get("egg_id").(int),
		"user":         resources.GetUserId(providerCtx),
		"docker_image": d.Get("docker_image").(string),
		"startup":      d.Get("startup_command").(string),
		"environment":  d.Get("environment_variables"),
		"limits": map[string]int{
			"memory": atoi(d.Get("limits.memory").(string)),
			"swap":   atoi(d.Get("limits.swap").(string)),
			"disk":   atoi(d.Get("limits.disk").(string)),
			"io":     atoi(d.Get("limits.io").(string)),
			"cpu":    atoi(d.Get("limits.cpu").(string)),
		},
		"feature_limits": map[string]int{
			"databases": atoi(d.Get("feature_limits.databases").(string)),
			"backups":   atoi(d.Get("feature_limits.backups").(string)),
		},
		"start_on_completion": d.Get("start_on_install").(bool),
		"allocation": map[string]int{
			"default": d.Get("allocation_id").(int),
		},
	}
	bodyJson, _ := json.Marshal(body)
	res, _ := axios.Request(&axios.Config{
		URL:     providerCtx.Url + "/api/application/servers",
		Method:  "POST",
		Headers: resources.GetHeaders(providerCtx.ApiKeys.AppKey),
		Body:    string(bodyJson),
	})
	if res.Status != 201 {
		return diag.FromErr(fmt.Errorf("the api returned status code %d with response:\n%s", res.Status, res.Data))
	}
	var response map[string]interface{}
	json.Unmarshal(res.Data, &response)
	d.SetId(strconv.Itoa(int(response["attributes"].(map[string]interface{})["id"].(float64))))
	serverResourceRead(ctx, d, providerCtx)
	return diag.Diagnostics{}
}
